const even = n => n % 2 === 0;

/* map */
const map = (f, lst) => {
  const o = [];
  for (const e of lst) o.push(f(e));

  return o;
};
console.log(map(even, [5, 6, 7]));

/* reduce */
const add = (x, y) => x + y;
const reduce = (f, init, lst) => {
  let result = init;
  for (const e of lst) result = f(result, e);

  return result;
};
console.log(reduce(add, 1, [1, 2]));

/* takewhile */
const takewhile = (f, lst) => {
  const a = [];
  for (const e of lst)
    if (f(e)) a.push(e);
    else return a;
};
console.log(takewhile(even, [2, 4, 5, 7, 8]));
console.log(takewhile(even, [1, 2, 3, 4]));

/* filter */
const filter = (pred, lst) => {
  const f = [];
  for (const e of lst) if (pred(e)) f.push(e);

  return f;
};

console.log(filter(even, [2, 4, 6, 8, 9]));

/* reverse */
const reverse = arr => {
  const r = [];
  for (let i = 0; i < arr.length; i++) r.push(arr[arr.length - 1 - i]);

  return r;
};
console.log(reverse([1, 2, 3, 4]));
/*dropwhile*/
const dropwhile = (f, lst) => {
  for (let i = 0; i < lst.length; i++)
    if (f(lst[i])) {
      if (!f(lst[i + 1])) return lst.slice(i + 1);
    } else return lst;
};

console.log(dropwhile(even, [6, 2, 4, 1, 3, 6, 8, 10]));
console.log(dropwhile(even, [6, 4, 2, 1]));
console.log(dropwhile(even, [1, 3, 5, 7]));

console.log(dropwhile(even, [2, 6, 8, 10]));

