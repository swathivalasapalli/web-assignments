/*clone*/
const clone = obj => {
  let o = {};
  for (const prop in obj) {
    o[prop] = obj[prop];
  }
  return o;
};
console.log(clone({ x: 1, y: 2 }));

/*invert*/
const props = {
  first: "alice",
  second: "jake"
};
const invert = obj => {
  let o = {};
  for (const prop in obj) {
    o[obj[prop]] = prop;
  }
  return o;
};
console.log(invert(props));
/*asssoc*/
const assoc = (k, v, obj) => {
  const o = clone(obj);
  o[k] = v;
  return o;
};
console.log(assoc("c", 3, { a: 1, b: 2 }));
/*dissoc*/
const dissoc = (obj, k) => {
  const r = {};
  for (const i in obj) {
    if (i != k) {
      r[i] = obj[i];
    }
  }
  return r;
};
console.log(dissoc({ a: 1, b: 2, c: 3 }, "a"));

