const factorial=(n)=>{
  let factorial=1;
  for(let i=1;i<=n;i++)
    {
      factorial=factorial*i;
    }
  return factorial;
}
console.log(factorial(5));


const ncr=(n,r)=>{
  return factorial(n)/(factorial(r)*factorial(n-r));
}
console.log(ncr(5,4));


const pascalline=(n)=>{
  const d=[];
  for(let i=0;i<=n;i++)
    {
      d.push(ncr(n,i));
     
       }
  return d;
}
console.log(pascalline(3));



const pascaltriangle=(n)=>{
  const arr=[];
    for(let j=1;j<=n;j++)
    {
     arr.push(pascalline(j));
    }
  return arr;
}
console.log(pascaltriangle(3));
