const pt = { x: 1, y: 2 };
const size = { width: 50, height: 60 };
const merge = (obj1, obj2) => {
  let o = {};
  for (const prop in obj1) {
    o[prop] = obj1[prop];
  }
  for (const prop in obj2) {
    o[prop] = obj2[prop];
  }
  return o;
};
console.log(merge(pt, size));

const splitat = (arr, index) => {
  const first = [];
  for (let i = 0; i < index; i += 1) {
    first.push(arr[i]);
  }
  const second = [];
  for (let i = index; i < arr.length; i++) {
    second.push(arr[i]);
  }
  return [first, second];
};
console.log(splitat([1, 2, 3, 4, 5], 2));

const prop = { x: 0, y: 2, z: 3 };
const pluck = (obj, props) => {
  let result = {};
  for (const prop of props) {
    result[prop] = obj[prop];
  }
  return result;
};
console.log(pluck(prop, ["x", "y"]));

const pick = (arr, prop) => {
  const a = [];
  for (const e in arr) {
    a.push(pluck(e));
  }
  return a;
};
console.log(pick([{ x: 1, y: 2, z: 3 }, { x: 5, y: 6, z: 7 }], ["x", "y"]));

