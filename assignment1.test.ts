import { fibonnaci } from './assignment2';
test('fibonnaci' , () => {
  expect(fibonnaci(6)).toEqual([1, 1, 2, 3 , 5 , 8]);
  expect(fibonnaci(6)).toEqual([1, 1, 2, 3 , 5 , 8, 13]);
  expect(fibonnaci(1)).toEqual([1, 1]);
  expect(fibonnaci(3)).toEqual([1, 1, 2]);
  expect(fibonnaci(7)).toEqual([1, 1, 2, 3 , 5 , 8, 13]);
  expect(fibonnaci(8)).toEqual([1, 1, 2, 3 , 5 , 8, 13]);
});
