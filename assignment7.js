/*interleave*/
const interleave = (lst1, lst2) => {
  const result = [];
  for (const i in lst1) {
    result.push(lst1[i]);
    result.push(lst2[i]);
  }
  return result;
};
console.log(interleave([1, 2, 3], [7, 5, 6]));

/* interpose */
const interpose = (lst, s) => {
  const re = [];
  for (const e in lst) re.push(lst[e], s);

  return re;
};
console.log(interpose([1, 2, 3, 4], ':'));

/* distinct */
const distinct = a => {
  const s = {};
  const result = [];
  let j = 0;
  for (let i = 0; i < a.length; i++) {
    const value = a[i];
    if (s[value] !== 1) {
      s[value] = 1;
      result[j++] = value;
    }
  }
  return result;
};
console.log(distinct([1, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6]));

