/*issorted*/
const isSorted = arr => {
  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] > arr[i + 1]) {
      return false;
    }
  }
  return true;
};
console.log(isSorted([1, 4, 6, 8, 9]));
console.log(isSorted([1, 3, 2, 4]));

/*any*/
const even = n => n % 2 === 0;
const any = (f, lst) => {
  let r = false;
  for (const e of lst) r = r || f(e);
  return r;
};

console.log(any(even, [7, 3, 4, 5]));
console.log(any(even, [2, 3, 5, 7]));
console.log(any(even, [1, 3, 5, 7]));

/* every */
const every = (f, lst) => {
  let a = true;
  for (const e of lst) a = a && f(e);
  return a;
};
console.log(every(even, [1, 2, 3, 4]));
console.log(every(even, [2, 4, 6, 8]));
console.log(every(even, [1, 3, 5, 7, 8]));
