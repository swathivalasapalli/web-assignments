/*repeat*/
const repeat = (n, x) => {
  const arr = [];
  for (let i = 1; i <= n; i++) {
    arr.push(x);
  }
  return arr;
};
console.log(repeat(5, "+"));

/*range*/
const range = (start, stop) => {
  const arr = [];
  for (let i = start; i < stop; i++) {
    arr.push(i);
  }
  return arr;
};
console.log(range(1, 5));

/*power*/
const pow = (x, y) => {
  let mul = 1;
  for (let i = 0; i < y; i++) {
    mul *= x;
  }
  return mul;
};
console.log(pow(2, 3));

/*square*/
const squareAll = arr => {
  const sq = [];
  for (let i = 0; i < arr.length; i++) {
    sq.push(arr[i] * arr[i]);
  }
  return sq;
};
console.log(squareAll([1, 2, 3, 4]));

/*sum*/

const sumall = arr => {
  let sum = 0;
  for (let i = 0; i < arr.length; i++) {
    sum += arr[i];
  }
  return sum;
};
console.log(sumall([3, 4, 5]));

/*even*/
const allEven = arr => {
  const ev = [];
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] % 2 === 0) {
      ev.push(arr[i]);
    }
  }
  return ev;
};
console.log(allEven([1, 2, 3, 6, 8]));

/*max*/
const maxall = arr => {
  let m = 0;
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] > m) m = arr[i];
  }
  return m;
};


/*index*/

const index = (arr, value) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === value) {
      return i;
    }
return -1;
    }
  }

console.log(index([1, 2, 3, 4, 5], 5));

console.log(index([1, 2, 3, 4, 5], 5));



  
