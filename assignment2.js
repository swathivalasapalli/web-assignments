/* take*/
const take = (n, arr) => {
  const a = [];
  for (let i = 0; i < n; i++) {
    a.push(arr[i]);
  }
  return a;
};
console.log(take(2, [1, 2, 3, 4]));

/*perfect*/

const isPerfect = num => {
  let sum = 0;
  let i = 1;
  while (i < num) {
    if (num % i === 0) sum+= i;
    i++;
  }
  if (sum === num) return true;
  else return false;
};
console.log(isPerfect(28));

/*reverse*/

const reverse = arr => {
  const r = [];
  for (let i = 0; i < arr.length; i++) {
    r.push(arr[arr.length - 1 - i]);
  }
  return r;
};
console.log(reverse([1, 2, 3, 43, 44]));

/*drop*/
const drop = (n, arr) => {
  const b = [];
  for (let i = n; i < arr.length; i++) {
    b.push(arr[i]);
  }
  return b;
};
console.log(drop(3, [1, 2, 3, 4, 5, 6]));

/*prime*/
const isPrime = n => {
  if (n === 1) return true;
  for (let i = 2; i < n; i++) {
    if (n % i === 0) return false;
  }
  return true;
};
console.log(isPrime(7));
/*concat*/
const concat = (arr1, arr2) => {
  for (let i = 0; i < arr2.length; i++) {
    arr1.push(arr2[i]);
  }
  return arr1;
};
console.log(concat([1, 2, 3], [4, 5, 6]));

